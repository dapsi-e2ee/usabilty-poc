#!/usr/bin/php

<?php

include_once("various_operations.php");

if ($argc != 2) {

    echo "Missing arguments, exiting.\n";
    exit();

} else $msg_to_encrypt = $argv[1];

echo "We're going to encrypt: $msg_to_encrypt\n\n";

echo "Loading Secret Key locally...\n";

if ($user_seckey = user_load("user_seckey")) {
    
    echo "--> SecKey: ".byte2hex($user_seckey)."\n\n";
    
    echo "Encrypting message...\n";

    // Using user key to encrypt information
    $nonce = "a nonce used for demo 12";

    $cipher_text = sodium_crypto_secretbox($msg_to_encrypt, $nonce, $user_seckey);
        
    echo "Encrypted message: ".byte2hex($cipher_text)."\n\n";
    echo "Saving encrypted message...";
    server_save("user_encrypted_message", $cipher_text);

} else {
    
    echo "## Secret Key is missing, cannot encrypt.\nPlease regenerate User Secret Key\n\n";
    exit();
    
}

/*
echo "Loading User Key share locally...\n";

if ($user_share = user_load("user_share"))
    echo "--> User share: ".byte2hex($user_share)."\n\n"; else
    echo "## User share is missing\n\n";

echo "Loading Server Key share from server...\n";

if ($server_share = server_load("server_share"))
    echo "--> Server share: ".byte2hex($server_share)."\n\n"; else
    echo "## Server share is missing\n\n";
*/



?>
