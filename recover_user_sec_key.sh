#!/usr/bin/php

<?php

include_once("various_operations.php");

$questions = array(
    "- what was your first pet name? ",
    "- what was your first phone number? ",
    "- what is your mother's year of birth? ",
    "- what is your favorite pin number? ",
    "- what is your favorite word? "
);

echo "Please answer the following quetsions:\n";

$answers = "";

for ($i=0;$i < count($questions);$i++) {

    echo $questions[$i];
    $input = rtrim(fgets(STDIN));
    $answers .= $input;
    
}

$user_seed_phrase = $answers;

$user_sha256_seed = hash('sha256',$user_seed_phrase, true);

echo strlen($user_seed_phrase)." user seed phrase: ".$user_seed_phrase."\n";
//echo strlen($user_sha256_seed)." user sha256 seed: ".$user_sha256_seed."\n";

$user_seed_key_pair = sodium_crypto_box_seed_keypair($user_sha256_seed);

$user_share1 = sodium_crypto_box_secretkey($user_seed_key_pair);

echo "--> User share1 from user seed: ".byte2hex($user_share1)."\n";

// Recovering user sec key

echo "Loading server share from server... \n";

if ($server_share = server_load("server_share")) {
    
    echo "--> Server Share: ".byte2hex($server_share)."\n\n";

    $user_sk_recover = $user_share1 ^ $server_share;

    echo "--> SecKey recover: ".byte2hex($user_sk_recover)."\n\n";

    echo "Saving User Secret Key locally...\n";
    user_save("user_seckey", $user_sk_recover);

    
    echo "Saving User Key share locally...\n";
    user_save("user_share", $user_share1);
    
} else {

    echo "Server share not found... sorry :( \n";
    exit();

}

?>
