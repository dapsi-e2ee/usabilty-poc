#!/usr/bin/php

<?php

include_once("various_operations.php");


while(!feof(STDIN)){
    
    $cipher_text = fgets(STDIN);
    
}


echo "We're going to decipher... ".byte2hex($cipher_text)."\n\n";

echo "Loading Secret Key locally...\n";

if ($user_seckey = user_load("user_seckey")) {
    
    echo "--> SecKey: ".byte2hex($user_seckey)."\n\n";
    
    // Using user key to encrypt information
    $nonce = "a nonce used for demo 12";
        
    $plaintext = sodium_crypto_secretbox_open($cipher_text, $nonce, $user_seckey);
    
    if ($plaintext === false) {
        
        throw new Exception("Bad cipher text");
        
    }

    echo "Deciphered message: ".$plaintext."\n\n";

} else {
    
    echo "## Secret Key is missing, cannot decrypt.\nPlease regenerate User Secret Key\n\n";
    exit();
    
}


?>
