<?php

function byte2hex($str) {

    $ret = "";

    for ($i=0;$i<strlen($str);$i++) {

        $ret .= bin2hex($str[$i]);
        
    }
    
    return $ret;
}

function user_save($filename, $binary_content) {

    $path = "./user/".$filename;
    
    if (!$fp = fopen($path, "w")) {
        echo "File open error\n";
        exit();
    }

    fwrite($fp, $binary_content);
    
    fclose($fp);

}

function server_save($filename, $binary_content) {

    $path = "./server/".$filename;
    
    if (!$fp = fopen($path, "w")) {
        echo "File open error\n";
        exit();
    }

    fwrite($fp, $binary_content);
    
    fclose($fp);

}

function user_load($filename) {

    $path = "./user/".$filename;
    
    if (!$fp = @fopen($path, "r")) {
        echo "File open error\n";
        return false;
    }

    $binary_content = fread($fp, filesize($path));
    
    fclose($fp);

    return $binary_content;

}

function server_load($filename) {

    $path = "./server/".$filename;
    
    if (!$fp = @fopen($path, "r")) {
        echo "File open error\n";
        return false;
    }

    $binary_content = fread($fp, filesize($path));
    
    fclose($fp);

    return $binary_content;

}



?>
